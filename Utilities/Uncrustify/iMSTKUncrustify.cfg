# Control what to do with the UTF-8 BOM (recommend 'remove')
utf8_bom                        = remove   # ignore/add/remove/force

# The number of columns to indent per level.
# Usually 2, 3, 4, or 8. Default=8
indent_columns                  = 4        # number

# The continuation indent. If non-zero, this overrides the indent of '(' and '=' continuation indents.
# For FreeBSD, this is set to 4. Negative value is absolute and not increased for each ( level
indent_continue                 = 0        # number

# How to use tabs when indenting code
# 0=spaces only
# 1=indent with tabs to brace level, align with spaces (default)
# 2=indent and align with tabs, using spaces when not on a tabstop
indent_with_tabs                = 0        # number

# Indent the code after an access specifier by one level.
# If set, this option forces 'indent_access_spec=0'
indent_access_spec_body         = true     # false/true

# True:  indent continued function call parameters one indent level
# False: align parameters under the open paren
indent_func_call_param          = true     # false/true

# Same as indent_func_call_param, but for function defs
indent_func_def_param           = false    # false/true

# Same as indent_func_call_param, but for function protos
indent_func_proto_param         = false    # false/true

# Same as indent_func_call_param, but for class declarations
indent_func_class_param         = false    # false/true

# Same as indent_func_call_param, but for class variable constructors
indent_func_ctor_var_param      = false    # false/true

# Same as indent_func_call_param, but for templates
indent_template_param           = false    # false/true

# Double the indent for indent_func_xxx_param options
indent_func_param_double        = false    # false/true

# True:  indent_func_call_param will be used (default)
# False: indent_func_call_param will NOT be used
use_indent_func_call_param      = false    # false/true

# Add or remove newline at the end of the file
nl_end_of_file                  = add      # ignore/add/remove/force

# The maximum consecutive newlines (3 = 2 blank lines)
nl_max                          = 3        # number

# Whether to remove blank lines after '{'
eat_blanks_after_open_brace     = true     # false/true

# Whether to remove blank lines before '}'
eat_blanks_before_close_brace   = true     # false/true

# Add or remove braces on single-line 'for' statement
mod_full_brace_for              = add      # ignore/add/remove/force

# Add or remove braces on single-line 'if' statement. Will not remove the braces if they contain an 'else'.
mod_full_brace_if               = add      # ignore/add/remove/force

# Add or remove braces on single-line 'while' statement
mod_full_brace_while            = add      # ignore/add/remove/force

# Whether to remove superfluous semicolons
mod_remove_extra_semicolon      = false    # false/true

# Add or remove newline between 'enum' and '{'
nl_enum_brace                   = add      # ignore/add/remove/force

# Add or remove newline between 'struct and '{'
nl_struct_brace                 = add      # ignore/add/remove/force

# Add or remove newline between 'union' and '{'
nl_union_brace                  = add      # ignore/add/remove/force

# Add or remove newline between 'if' and '{'
nl_if_brace                     = add      # ignore/add/remove/force

# Add or remove newline between '}' and 'else'
nl_brace_else                   = add      # ignore/add/remove/force

# Add or remove newline between 'else' and '{'
nl_else_brace                   = add      # ignore/add/remove/force

# Add or remove newline between 'else' and 'if'
nl_else_if                      = remove   # ignore/add/remove/force

# Add or remove newline between '}' and 'finally'
nl_brace_finally                = add      # ignore/add/remove/force

# Add or remove newline between 'finally' and '{'
nl_finally_brace                = add      # ignore/add/remove/force

# Add or remove newline between 'try' and '{'
nl_try_brace                    = add      # ignore/add/remove/force

# Add or remove newline between 'for' and '{'
nl_for_brace                    = add      # ignore/add/remove/force

# Add or remove newline between 'catch' and '{'
nl_catch_brace                  = add      # ignore/add/remove/force

# Add or remove newline between '}' and 'catch'
nl_brace_catch                  = add      # ignore/add/remove/force

# Add or remove newline between 'do' and '{'
nl_do_brace                     = add      # ignore/add/remove/force

# Add or remove newline between '}' and 'while' of 'do' statement
nl_brace_while                  = add      # ignore/add/remove/force

# Add or remove newline between 'switch' and '{'
nl_switch_brace                 = add      # ignore/add/remove/force

# Add or remove newline between function signature and '{'
nl_fdef_brace                   = add      # ignore/add/remove/force

# Don't split one-line braced statements inside a class xx { } body
nl_class_leave_one_liners       = true     # false/true

# Don't split one-line C++11 lambdas - '[]() { return 0; }'
nl_cpp_lambda_leave_one_liners  = true     # false/true

# Add or remove space around compare operator '<', '>', '==', etc
sp_compare                      = add      # ignore/add/remove/force

# If true, cpp lambda body will be indentedDefault=False.
indent_cpp_lambda_body          = false    # false/true

# Add or remove space around assignment operator '=', '+=', etc
sp_assign                       = add      # ignore/add/remove/force

# Add or remove space between '>' and '>' in '>>' (template stuff C++/C# only). Default=Add
sp_angle_shift                  = remove   # ignore/add/remove/force

# Permit removal of the space between '>>' in 'foo<bar<int> >' (C++11 only). Default=False
# sp_angle_shift cannot remove the space without this option.
sp_permit_cpp11_shift           = true     # false/true

# Try to limit code width to N number of columns
code_width                      = 200      # number

# If false, disable all multi-line comment changes, including cmt_width. keyword substitution and leading chars.
# Default=True.
# If enabled in IMSTK, it interferes with license header
cmt_indent_multi                = false    # false/true

# The filename that contains text to insert at the head of a file if the file doesn't start with a C/C++ comment.
# Will substitute $(filename) with the current file's name.
cmt_insert_file_header          = "Utilities/HeaderForSources.txt"